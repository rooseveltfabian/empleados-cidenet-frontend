import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
// Rutas
import { APP_ROUTING } from './app.routes';

// Servicios
import { EmpleadosService } from './servicios/empleados.service';

// Componentes
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { EmpleadosComponent } from './components/empleados/empleados.component';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { EmpleadoTarjetaComponent } from './components/empleado-tarjeta/empleado-tarjeta.component';
import { PaginacionComponent } from './components/paginacion/paginacion.component';
import { NuevoEmpleadoComponent } from './page/nuevo-empleado/nuevo-empleado.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    EmpleadosComponent,
    BuscadorComponent,
    EmpleadoTarjetaComponent,
    PaginacionComponent,
    NuevoEmpleadoComponent,

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    APP_ROUTING
  ],
  providers: [
    EmpleadosService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
