import { Component, OnInit } from '@angular/core';
import { EmpleadosService } from '../../servicios/empleados.service';
import { Empleado } from 'src/app/interfaces/empleado.interface';
import { connectableObservableDescriptor } from 'rxjs/internal/observable/ConnectableObservable';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html'
})
export class EmpleadosComponent implements OnInit {


  empleados: Empleado[] = [];
  indice = 0;
 
  paginacion = {
    numeroPaginas: 0,
    indiceActivo: 1,
    gruposPaginas: [],
    indiceGrupo: 0,
    cantidadBotonesPaginacion: 5
  }

  cantidaTotalEmpelados = 0;
  constructor(private _empleadosService: EmpleadosService
  ) {
  }

  ngOnInit() {
    this.empleados = [];
    let getEmp = this._empleadosService.getEmpleados(this.indice)
      .subscribe((res: any) => {
        this.empleados = res.empleados;

        let countemp= this._empleadosService.countEmpleados()
        .subscribe((res: any) => {
          if (res.status) {
            this.cantidaTotalEmpelados = res.cantidad;
            this.paginacion.numeroPaginas = Math.ceil(this.cantidaTotalEmpelados / this.empleados.length);
            this.paginacion.gruposPaginas = this.construirArrayGrupo(this.paginacion.numeroPaginas , this.paginacion.cantidadBotonesPaginacion)

            countemp.unsubscribe()
          }
        })


        getEmp.unsubscribe();
      })

    
  }



  irPagina(indice){
    this.consultarDatosPagina(indice);
    this.paginacion.indiceActivo=indice+1;
  }

  consultarDatosPagina(i:number){
    this._empleadosService.getEmpleados(i).subscribe((res:any)=>{
      if(res.status){
        this.empleados = res.empleados
      }
    })
  }

  nextPage() {
    if (this.paginacion.indiceActivo < this.paginacion.numeroPaginas) {
      this.paginacion.indiceActivo++;
    }

    let arrGrupoSeleccionado = this.paginacion.gruposPaginas[this.paginacion.indiceGrupo]

    if (this.paginacion.indiceActivo > arrGrupoSeleccionado[arrGrupoSeleccionado.length-1] && this.paginacion.indiceGrupo < this.paginacion.numeroPaginas) {
      this.paginacion.indiceGrupo++;
    }
    console.log(this.paginacion)
    this.consultarDatosPagina(this.paginacion.indiceActivo-1);
  }

  previousPage(){
    if (this.paginacion.indiceActivo > 1) {
      this.paginacion.indiceActivo--;
    }

    let arrGrupoSeleccionado = this.paginacion.gruposPaginas[this.paginacion.indiceGrupo]

    if (this.paginacion.indiceActivo < arrGrupoSeleccionado[0] && this.paginacion.indiceGrupo > 0) {
      this.paginacion.indiceGrupo--;
    }
    console.log(this.paginacion)
    this.consultarDatosPagina(this.paginacion.indiceActivo-1);
  }

  construirArrayGrupo(cantidadDatos:number, cantidadGrupo:number) {
    //contruye un array bidimencional para separar los grupos de paginacion a mmostrar. 
    //EJ: si son 6 paginas que debe mostar entonces se crea un array como este [ [1,2,3], [4,5,6] ] 
    let arr = [];
    let pos = -1
    console.log("los datos que llegan construirArrayGrupo: ", cantidadDatos, cantidadGrupo)
    for (let i = 0; i < (cantidadDatos / cantidadGrupo); i++) { 

      arr.push([]);
      pos++;

      for (let j = i * cantidadGrupo, x = 0; j < (i * cantidadGrupo + cantidadGrupo); j++, x++) {
        if (j === cantidadDatos) {
          break;
        }
        arr[pos][x] = j + 1;
      }

    }
    return arr;
  }


}
