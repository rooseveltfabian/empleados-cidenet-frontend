import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EmpleadosService } from '../../servicios/empleados.service';

@Component({
  selector: 'app-buscador',
  templateUrl: './buscador.component.html'
})
export class BuscadorComponent implements OnInit {

  empleados: any[] = []
  termino: string;


  constructor(
    private activatedRoute: ActivatedRoute,
    private _empleadosService: EmpleadosService,
    private router:Router
    ) {

  }

  /**
   * si el usuario ingresaa un termino en la busqueda entonces lo consulta
   * si el usuario no ingresa ningun valor entonces redirige a la lista de empleados
   */
  ngOnInit() {

    this.activatedRoute.params.subscribe(params => {
      this.termino = params['termino'];
      if(this.termino.trim() === ""){
        this.router.navigate( ['/empleados'] );
      }else{
        this._empleadosService.buscarEmpleado(params['termino']).subscribe((res: any) => {
          if (res.status) {
            this.empleados = res.empleados;
          }
        })
      }
    });
  }



}
