import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Empleado } from 'src/app/interfaces/empleado.interface';


@Component({
  selector: 'app-paginacion',
  templateUrl: './paginacion.component.html',
  styleUrls: ['./paginacion.component.css']
})
export class PaginacionComponent implements OnInit {

  @Input() empleados: Empleado[];
  @Input() paginacion: any;

  @Output() irpagina: EventEmitter<any>;
  @Output() previouspage: EventEmitter<any>;
  @Output() nextpage: EventEmitter<any>;
  



  constructor() { 
    this.irpagina = new EventEmitter;
    this.previouspage = new EventEmitter;
    this.nextpage = new EventEmitter;
  }

  ngOnInit() {
   
  }

  irPagina( i ){
    this.irpagina.emit(i);
  }

  previousPage(){
    this.previouspage.emit();
  }

  nextPage(){
    this.nextpage.emit();
  }

 

}
