import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Empleado } from 'src/app/interfaces/empleado.interface';
import { Router } from '@angular/router';
import { EmpleadosService } from 'src/app/servicios/empleados.service';

@Component({
  selector: 'app-empleado-tarjeta',
  templateUrl: './empleado-tarjeta.component.html',
  styleUrls: ['./empleado-tarjeta.component.css']
})
export class EmpleadoTarjetaComponent implements OnInit {

  @Input() empleado: Empleado;
  @Input() index: number;
  @Output() cosultaEmpleados: EventEmitter<any>;

  constructor(
    private router: Router,
    private _empleadosService: EmpleadosService) {
    this.cosultaEmpleados = new EventEmitter;
  }

  ngOnInit() {
  }


  /**
   * funcion para enviar a editar el empleado
   */
  editarEmpleado(){
    console.log("editar: ",this.empleado.id)
    this.router.navigate( ['/editar-empleado'],{queryParams: this.empleado });
  }


/**
   * funcion para elimiar el empleado
   */
  eliminarEmpleado(id: number){

    this._empleadosService.eliminarEmpleado(id).subscribe((res:any)=>{
      if(res.status){
        console.log("se elimino empleado")
        this.router.navigate( ['/empleados']);
      }
      this.cosultaEmpleados.emit()

      console.log("temino de eliminar", res)
    })
  }

}
