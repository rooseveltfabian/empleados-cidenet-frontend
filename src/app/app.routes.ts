
import { RouterModule, Routes } from '@angular/router';
import { EmpleadosComponent } from './components/empleados/empleados.component';
import { BuscadorComponent } from './components/buscador/buscador.component';
import { NuevoEmpleadoComponent } from './page/nuevo-empleado/nuevo-empleado.component';


const APP_ROUTES: Routes = [
  { path: 'empleados', component: EmpleadosComponent },
  { path: 'nuevo-empleado', component: NuevoEmpleadoComponent },
  { path: 'editar-empleado', component: NuevoEmpleadoComponent },
  { path: 'buscar/:termino', component: BuscadorComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'empleados' }
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES, {useHash:true});
