
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Empleado } from '../interfaces/empleado.interface'

@Injectable()
export class EmpleadosService {

  url = "http://localhost:3000";

  constructor(private http: HttpClient) {
  }


  /**
   * observable para consultar empleados por indice para la paginacion
   * @param indice numero de la pagina inicia en cero
   * @returns observable con el los empleados 
   */
  getEmpleados(indice:any){
   indice = indice.toString();
   return this.http.get(this.url+"/empleados/listar/pagina", { params:{ indice } } );
  }


  /**
   * cuenta el numero de empleados en el sistema
   * @returns observable con la cantida de empleados
   */
  countEmpleados(){
    return this.http.get(this.url+"/empleados/cantidad");
  }
  
  /**
   * observable para consultar los empleados para el buscador
   * @param consulta string a buscar
   * @returns  observable de la busqueda
   */
  buscarEmpleado( consulta:string ) {
    return this.http.get(this.url+"/empleados/filtrar", { params:{ consulta } } );
  }

  /**
   * observable para guardar un empleado 
   * @param empleado  objeto empleado 
   * @returns observable con el status de la insercion
   */
  guardarEmpleado( empleado:any ) {
    return this.http.post(this.url+"/empleados/insertar", empleado  );
  }

  /**
   * observable para editar un empleado 
   * @param empleado objeto empleado con su id
   * @returns observable con el status de la edicion y el correo si fue modificado el nombre y apellido
   */
  editarEmpleado( empleado:any ) {
    return this.http.put(this.url+"/empleados/editar", empleado  );
  }
  
  /***
   * observable que elimina un empleado por id
   */
  eliminarEmpleado(id: any){
    return this.http.delete(this.url+"/empleados/eliminar", { params:{ id }} );
  }


}


