export interface Empleado{
    id: number;
    primer_apellido: string;
    segundo_apellido: string;
    primer_nombre: string;
    otros_nombres: string;
    id_pais_empleo: number;
    id_tipo_identificacion: number;
    no_identificacion: string;
    correo: string;
    fecha_ingreso: string;
    id_area: number;
    estado: string;
    fecha_hora_registro: string;
    fecha_edicion?: string;
    pais_empleo: string;
    tipo_identificacion: string;
    area: string;
}
