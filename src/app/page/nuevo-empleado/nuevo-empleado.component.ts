import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Empleado } from 'src/app/interfaces/empleado.interface';
import { EmpleadosService } from 'src/app/servicios/empleados.service';
import * as moment from "moment";


@Component({
  selector: 'app-nuevo-empleado',
  templateUrl: './nuevo-empleado.component.html',
  styleUrls: ['./nuevo-empleado.component.css']
})
export class NuevoEmpleadoComponent implements OnInit {

  empleado :any;
  editando: boolean = false;

  guardando = false;
  errorGuardando = false;
  mensajeError:string="";
  exitoGuardando = false;
  forma: FormGroup
  constructor(
    private fb: FormBuilder,
    private _empleadosService: EmpleadosService,
    private activatedRoute: ActivatedRoute) {

    this.crearFormulario();
    this.cargarDataAlFormulario();

  }

  ngOnInit(): void {

  }

/**
 * funciones para validar la informacion digitada por el usuario
 */
  get nombreNoValido() {
    return this.forma.get('primer_nombre').invalid && this.forma.get('primer_nombre').touched
  }

  get otrosNombresNoValido() {
    return this.forma.get('otros_nombres').invalid && this.forma.get('otros_nombres').touched
  }

  get primerApellidoNoValido() {
    return this.forma.get('primer_apellido').invalid && this.forma.get('primer_apellido').touched
  }

  get segundoApellidoNoValido() {
    return this.forma.get('segundo_apellido').invalid && this.forma.get('segundo_apellido').touched
  }

  get paisNoValido(){
    return this.forma.get('id_pais_empleo').hasError('required') && this.forma.get('id_pais_empleo').touched
  
  }

  get tipoIdentificacionNoValido(){
    return this.forma.get('id_tipo_identificacion').hasError('required') && this.forma.get('id_tipo_identificacion').touched
  }

  get no_identificacionNoValido(){
    return this.forma.get('no_identificacion').invalid && this.forma.get('no_identificacion').touched
  }

  get fecha_ingresoNoValido(){

    var fechaInicio = new Date( this.forma.get('fecha_ingreso').value+"").getTime();
    var fechaFin    = new Date().getTime();

    var diff = fechaFin - fechaInicio;


    let fechaInValida= false;
    let diferencia:any = diff/(1000*60*60*24)
    if(isNaN(diferencia) || diferencia > 30 || diferencia <0 ){
      fechaInValida= true;
    }


    return this.forma.get('fecha_ingreso').invalid && this.forma.get('fecha_ingreso').touched || fechaInValida;
  }

  get areaNoValido(){
    return this.forma.get('id_area').hasError('required') && this.forma.get('id_area').touched
  }

  /************************************************************************************************************* */

 

  /**
  * funcion para construir el formulario
  */
  crearFormulario() {

    this.forma = this.fb.group({
      primer_nombre: ['', [Validators.required, Validators.pattern('^[A-Z /S/]+$')] ],
      otros_nombres: ['', [Validators.maxLength(50), Validators.pattern('^[A-Z /S/]+$')] ],
      primer_apellido: ['', [Validators.required, Validators.pattern('^[A-Z /S/]+$')] ],
      segundo_apellido: ['', [Validators.maxLength(20),Validators.pattern('^[A-Z /S/]+$')] ],
      correo: ['', [] ],
      id_pais_empleo: ['', [ Validators.required ] ],
      id_tipo_identificacion: ['', [ Validators.required ] ],
      no_identificacion: ['', [Validators.required, Validators.pattern('^[A-Za-z0-9-]+$')]],
      fecha_ingreso: ['', [Validators.required, Validators.pattern('^([0-9]{4}[-]?((0[13-9]|1[012])[-]?(0[1-9]|[12][0-9]|30)|(0[13578]|1[02])[-]?31|02[-]?(0[1-9]|1[0-9]|2[0-8]))|([0-9]{2}(([2468][048]|[02468][48])|[13579][26])|([13579][26]|[02468][048]|0[0-9]|1[0-6])00)[-]?02[-]?29)$')]],
      id_area: ['', [ Validators.required ] ],
    }, {
    });

  }



/**
 * Funcion para cargar los datos al formulario
 */
  cargarDataAlFormulario() {
    let cargarData= 
      {
        primer_nombre: this.activatedRoute.snapshot.queryParamMap.get('primer_nombre'),
        otros_nombres: this.activatedRoute.snapshot.queryParamMap.get('otros_nombres'),
        primer_apellido: this.activatedRoute.snapshot.queryParamMap.get('primer_apellido'),
        segundo_apellido: this.activatedRoute.snapshot.queryParamMap.get('segundo_apellido'),
        correo : this.activatedRoute.snapshot.queryParamMap.get('correo'),
        id_pais_empleo: this.activatedRoute.snapshot.queryParamMap.get('id_pais_empleo'),
        id_tipo_identificacion: this.activatedRoute.snapshot.queryParamMap.get('id_tipo_identificacion'),
        no_identificacion: this.activatedRoute.snapshot.queryParamMap.get('no_identificacion'),
        id_area: this.activatedRoute.snapshot.queryParamMap.get('id_area'),
        fecha_ingreso: this.activatedRoute.snapshot.queryParamMap.get('fecha_ingreso')
     }
     console.log("CORREO: ", this.activatedRoute.snapshot.queryParamMap.get('correo'))

     this.forma.reset(cargarData);

     this.empleado= {...cargarData};
     this.empleado.id= this.activatedRoute.snapshot.queryParamMap.get('id')

     if(this.empleado.id > 0){
      this.editando = true;
     }else{
      this.forma.reset({fecha_ingreso: moment().format('YYYY-MM-DD')});
     }
     console.log("cargarData: ",cargarData)
     console.log("empleado: ", this.empleado)

    

  }


  /**
   * funcion para guardar los datos en el servidor
   */

  guardar() {

    if (this.forma.invalid) {

      return Object.values(this.forma.controls).forEach((control: any) => {

        if (control instanceof FormGroup) {
          Object.values(control.controls).forEach(control => control.markAsTouched());
        } else {
          control.markAsTouched();
        }


      });

    }

    if(this.activatedRoute.snapshot.queryParamMap.get('id')){ 
      //Al EDITAR
      this.guardando= true;
      console.log(this.forma.value)
      let emp = {...this.forma.value}
      emp.id = this.activatedRoute.snapshot.queryParamMap.get('id')
     let editar = this._empleadosService.editarEmpleado(emp).subscribe((res:any)=>{
        if(res.status){
          this.exitoGuardando = true;
          if(res.correo){

            this.forma.patchValue({correo: res.correo})
          }
          setTimeout(() => {
            this.exitoGuardando = false;
          }, 5000);
        }else{
          this.errorGuardando =true;
          setTimeout(() => {
            this.errorGuardando = false;
          }, 5000);
        }
        editar.unsubscribe();
        this.guardando = false;
        console.log("respuesta: ",res)
      })

    }else{
      //Al INSERTAR
      this.guardando= true;
      let inser = this._empleadosService.guardarEmpleado(this.forma.value).subscribe((res:any)=>{
        if(res.status){
          this.forma.reset({fecha_ingreso: moment().format('YYYY-MM-DD')});
          this.exitoGuardando = true;
          setTimeout(() => {
            this.exitoGuardando = false;
          }, 5000);
        }else{
          (res.msg && res.msg.length > 0)? this.mensajeError = res.msg : null;
          this.errorGuardando =true;
          setTimeout(() => {
            this.errorGuardando = false;
          }, 5000);
        }
        inser.unsubscribe();
        this.guardando = false;
        console.log("respuesta: ",res)
      })

    }

  }


}
