import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {

  constructor( private router:Router ) { }

  ngOnInit() {
  }

  /**
   * funcion para enviar a buscar un consulta
   * @param termino recibe la consulta del usuario a buscar
   */
  buscarEmpleado( termino:string ){
    this.router.navigate( ['/buscar',termino] );
  }

}
